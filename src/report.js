import pretty from 'pretty';





const routeToNode = (doc, route) => {
  let ref = doc;
  route.forEach(i => {
    ref = ref.childNodes[i];
  });
  return ref;
};

const serializeTextNodeObj = (nodeObj) => {
  return nodeObj.data;
};

const serializeCommentNodeObj = (nodeObj) => {
  return `<!--${nodeObj.data}-->`;
};

const serializeElementNodeObj = (nodeObj) => {

  if (typeof nodeObj.attributes === 'undefined')
    nodeObj.attributes = {};

  if (typeof nodeObj.childNodes === 'undefined')
    nodeObj.childNodes = [];

  const tagName = nodeObj.nodeName.toLowerCase();
  const attrs = Object.keys(nodeObj.attributes)
    .map(name => `${name}="${nodeObj.attributes[name]}"`);

  const head = [tagName].concat(attrs).join(' ');
  const children = nodeObj.childNodes.map(serializeNodeObj).join('');

  return `<${head}>${children}</${tagName}>`;
};

const serializeNodeObj = (nodeObj) => {

  if (nodeObj.nodeName === '#text')
    return serializeTextNodeObj(nodeObj);

  if (nodeObj.nodeName === '#comment')
    return serializeCommentNodeObj(nodeObj);

  if (nodeObj.nodeName[0] !== '#')
    return serializeElementNodeObj(nodeObj);

  return `serializeNodeObj(${JSON.stringify(nodeObj)})`;
};

// const mdHTML = pretty
const mdHTML = input => {

  return `\n${pretty(input)}\n`;
};

const renderMismatch = (diff, actualDoc, expectedDoc) => {

  if (diff.action === 'addElement') {
    const snippet = mdHTML(serializeNodeObj(diff.element));
    return `Extra element:\n${snippet}`;
  }

  if (diff.action === 'removeElement') {
    const snippet = mdHTML(serializeNodeObj(diff.element));
    return `Missing element:\n${snippet}`;
  }

  if (diff.action === 'removeClass') {
    const node = routeToNode(actualDoc, diff.route) || '???';
    // debugger
    const snippet = mdHTML(node.outerHTML || node.toString());
    return `Missing class \`${diff.name}\` on element:\n${snippet}`;
  }

  if (diff.action === 'removeAttribute') {
    const node = routeToNode(actualDoc, diff.route) || '???';
    const attr = `${diff.name}="${diff.value}"`;
    const snippet = mdHTML(node.outerHTML || node.toString());
    return `Missing attribute \`${attr}\` on element:\n${snippet}`;
  }

  if (diff.action === 'addAttribute') {
    const node = routeToNode(actualDoc, diff.route) || '???';
    const attr = `${diff.name}="${diff.value}"`;
    const snippet = mdHTML(node.outerHTML || node.toString());
    return `Extra attribute \`${attr}\` on element:\n${snippet}`;
  }

  if (diff.action === 'replaceElement') {
    const eSnippet = mdHTML(serializeNodeObj(diff.oldValue));
    const aSnippept = mdHTML(serializeNodeObj(diff.newValue));
    return `Element mismatch:\n\nExpected:\n${eSnippet}\nActual:\n${aSnippept}`;
  }

  if (diff.action === 'modifyAttribute') {
    const node = routeToNode(expectedDoc, diff.route) || '???';
    const htmlSnippet = mdHTML(node.outerHTML || node.toString());
    const eSnippet = diff.oldValue;
    const aSnippet = diff.newValue;
    return `HTML mismatch on attribute \`${diff.name}\` in element\n${htmlSnippet}\n` +
      `Expected\n${eSnippet}\nbut got\n${aSnippet}`;
  }

  if (diff.action === 'addTextElement')
    return `Extra text\n${diff.value}`;

  if (diff.action === 'removeTextElement')
    return `Missing text\n${diff.value}`;

  if (diff.action === 'modifyTextElement') {
    const eSnippet = diff.oldValue;
    const aSnippet = diff.newValue;
    return `Text mismatch:\n\nExpected:\n${eSnippet}\nActual:\n${aSnippet}`;
  }

  return `renderMismatch(${JSON.stringify(diff)})`;
};

export const reportMismatches = (diffs, actualDoc, expectedDoc) => {
  const msg = diffs
    .map(d => renderMismatch(d, actualDoc, expectedDoc))
    .join('\n\n\n');
  throw new Error(msg);
};