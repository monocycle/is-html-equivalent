import jsdom from 'jsdom';
import domWalk from 'domwalk';
import { makeDiff } from './diff';
import { reportMismatches } from './report';





const trimTextNode = node => {

  if (node.nodeType !== 3)
    return;

  const trimmedText = node.textContent.trim();

  if (trimmedText.length === 0)
    return void node.parentNode.removeChild(node);

  node.textContent = trimmedText;
};

const makeDocsAndDiff = (actual, expected) => {

  const actualDoc = jsdom.jsdom(actual);

  domWalk(actualDoc, trimTextNode);

  const expectedWithWildcards = expected
    .replace(/{{[^}]*}}/g, '<!--$ignored-wildcard-element$-->');
  const expectedDoc = jsdom.jsdom(expectedWithWildcards);

  domWalk(expectedDoc, trimTextNode);

  const diffs = makeDiff(actualDoc, expectedDoc);

  return [
    diffs,
    actualDoc,
    expectedDoc
  ];
};

export const assertHtmlIsEquivalent = (actual, expected) => {

  const [diffs, actualDoc, expectedDoc] = makeDocsAndDiff(actual, expected);

  if (diffs.length > 0)
    reportMismatches(diffs, actualDoc, expectedDoc);
};

export const isHtmlEquivalent = (actual, expected) => {

  const [diffs] = makeDocsAndDiff(actual, expected);

  return diffs.length === 0;
};
