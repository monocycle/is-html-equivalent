import test from 'ava';
import { assertHtmlIsEquivalent } from './index';





test('should detect basic attribute missing', t => {
  const actual = `
      <div class="fe10c23a">
        <h1 class="aab058a7">This is a title</h1>
        <p>This is some text content</p>
      </div>
    `;

  const expected = `
      <div class="...">
        {{ ... }}
        <p class="highlighted">This is some text content</p>
        {{ ... }}
      </div>
    `;

  try {
    assertHtmlIsEquivalent(actual, expected);
  } catch (err) {
    t.is(err.message,
      'Missing attribute `class="highlighted"` on element:\n' +
      '\n' +
      '<p>This is some text content</p>\n'
    );
  }
});

