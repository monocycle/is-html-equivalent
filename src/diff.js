import DiffDOM from 'diff-dom';
import stableSort from 'lodash.sortby';





const dd = new DiffDOM();

const areRoutesAdjacent = (diffA, diffB) => {
  const routeA = diffA.route.join('');
  const routeB = diffB.route.join('');
  const prefixA = routeA.substring(0, routeA.length - 1);
  const prefixB = routeB.substring(0, routeB.length - 1);
  const lastA = diffA.route[diffA.route.length - 1];
  const lastB = diffB.route[diffB.route.length - 1];
  return routeA.length === routeB.length
    && prefixA === prefixB
    && (lastA === lastB || lastA + 1 === lastB);
};

const groupByWildcard = (groups, diff/* , index, diffs */) => {
  const group = groups[groups.length - 1];
  const firstInGroup = group[0];
  const lastInGroup = group[group.length - 1];
  if (firstInGroup
    && isRemoveWildcardDiff(firstInGroup)
    && lastInGroup
    && areRoutesAdjacent(lastInGroup, diff)
    && ['addTextElement', 'addElement'].includes(diff.action)) {
    group.push(diff);
  }
  else {
    groups.push([diff]);
  }
  return groups;
};

const isWildcardComment = (element) => {
  return element.nodeName === '#comment'
    && element.data.trim() === '$ignored-wildcard-element$';
};

const isRemoveWildcardDiff = (diff) => {
  return diff.action === 'removeElement' && isWildcardComment(diff.element);
};

export const makeDiff = (actualDoc, expectedDoc) => {

  const diffs1 = dd.diff(expectedDoc, actualDoc);

  const diffs2 = stableSort(diffs1, (d) => d.route.join(''));

  const diffs = diffs2
    .reduce((before, diff) => { // splitReplaceDiff

      if (diff.action === 'replaceElement' && isWildcardComment(diff.oldValue))
        return [
          ...before,
          { // removeDiff
            action: 'removeElement',
            route: diff.route,
            element: diff.oldValue,
          },
          { // addDiff
            action: 'addElement',
            route: diff.route,
            element: diff.newValue,
          }
        ];

      if (diff.action === 'modifyAttribute'
        && diff.name === 'class'
        && diff.oldValue.includes('...')) {

        const actualClasses = diff.newValue.split(' ');

        return [
          ...before,
          ...diff.oldValue
            .split(' ')
            .filter(className => className !== '...' && !actualClasses.includes(className))
            .map(className => ({
              action: 'removeClass',
              route: diff.route,
              name: className,
            }))
        ];
      }

      return [
        ...before,
        diff
      ];
    }, [])
    .reduce(groupByWildcard, [[]])
    .filter(group => group.length > 0 && !isRemoveWildcardDiff(group[0]))
    .reduce((a, b) => a.concat(b), []);
  return diffs;
};
